﻿using AutoBot_BatchRegister.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Reflection;

namespace AutoBot_BatchRegister.Utility
{
    public class UFAAPI
    {
        UtilityClass ut = new UtilityClass();
        public string url_api()
        {
            string url = string.Empty;
            using (cn_systemEntities db = new cn_systemEntities())
            {
                url = db.cmn_config.Where(x => x.code_config.Equals("URL_API_UFA")).Select(x => x.value_config).FirstOrDefault();
            }
            return url;
        }
        public string NewUser()
        {
            string user_ufabet = String.Empty;
            string new_user_ufabet = String.Empty;
            string agent = ConfigurationManager.AppSettings["agent_name"];

            using (cn_systemEntities db = new cn_systemEntities())
            {
                var max = db.tb_m_customer_user.Where(x => x.user_ufabet.Contains(agent)).Max(x => x.user_ufabet);
                if (max != null)
                {
                    user_ufabet = max;
                    user_ufabet = user_ufabet.Substring(user_ufabet.Length - 6);
                    String s = user_ufabet.TrimStart(new Char[] { '0' });
                    int calulate = Convert.ToInt32(s) + 1;
                    string queue_ufabet = calulate.ToString("000000");

                    bool isNoQueue = false;
                    while (!isNoQueue)
                    {
                        var user_onqueue = db.tb_r_register_queue.Where(f => f.user_ufabet.Equals(queue_ufabet)).FirstOrDefault();
                        if (user_onqueue != null)
                        {
                            calulate = calulate + 1;
                            queue_ufabet = calulate.ToString("000000");
                        }
                        else
                        {
                            isNoQueue = true;
                        }
                    }

                    tb_r_register_queue addQueue = new tb_r_register_queue();
                    addQueue.user_ufabet = calulate.ToString("000000");
                    db.tb_r_register_queue.Add(addQueue);
                    db.SaveChanges();

                    new_user_ufabet = calulate.ToString("000000");
                }
                else
                {
                    new_user_ufabet = "001000";
                }

            }

            return new_user_ufabet;
        }
        public bool Register(string agent, string Username, string Password, string first_deposit, string contact)
        {
            string url = url_api() + "register";
            bool result = false;
            try
            {
                string json = "{\"user_agent\":\"" + agent + "\"," +
                                              "\"user_name\":\"" + Username + "\"," +
                                               "\"password\":\"" + Password + "\"," +
                                                "\"first_deposit\":\"" + first_deposit + "\"," +
                                              "\"contact\":\"" + contact + "\"}";

                using (var client = new HttpClient())
                {
                    var response = client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json")).GetAwaiter().GetResult();

                    string responseString = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                    var jsonResult = JsonConvert.DeserializeObject(responseString).ToString();
                    var resultJson = JsonConvert.DeserializeObject<JsonResultItem>(jsonResult);
                    result = Convert.ToBoolean(resultJson.result);
                }

            }
            catch (Exception ex)
            {
                ut.InsertErrorLog(ex, MethodBase.GetCurrentMethod().ReflectedType.Name, MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public string url_reset()
        {
            string url_reset = string.Empty;
            using (cn_systemEntities db = new cn_systemEntities())
            {
                url_reset = db.cmn_config.Where(x => x.code_config.Equals("URL_API_RESET_UFA")).Select(x => x.value_config).FirstOrDefault();
            }
            return url_reset;
        }
        public bool ResetPassword(string agent, string user_ufabet, string old_password, string new_password)
        {
            string url = url_reset();
            bool result = false;
            try
            {
                if (!string.IsNullOrEmpty(url))
                {
                    string json = "{\"user_agent\":\"" + agent + "\"," +
                                "\"user_ufabet\":\"" + user_ufabet + "\"," +
                                 "\"old_password\":\"" + old_password + "\"," +
                                  "\"new_password\":\"" + new_password + "\"}";

                    using (var client = new HttpClient())
                    {
                        var response = client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json")).GetAwaiter().GetResult();

                        string responseString = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                        var jsonResult = JsonConvert.DeserializeObject(responseString).ToString();
                        var resultJson = JsonConvert.DeserializeObject<JsonResultItem>(jsonResult);
                        result = Convert.ToBoolean(resultJson.result);
                    }
                }
            }
            catch (Exception ex)
            {
                ut.InsertErrorLog(ex, MethodBase.GetCurrentMethod().ReflectedType.Name, MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}
