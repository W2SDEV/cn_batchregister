﻿using AutoBot_BatchRegister.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBot_BatchRegister.Utility
{
    public class UtilityClass
    {
        public void ErrorLog(Exception ex)
        {
            try
            {
                using (cn_systemEntities db = new cn_systemEntities())
                {
                    tb_m_error_log error = new tb_m_error_log();
                    error.error_name = ex.Source + ": " + ex.Message;
                    error.error_desc = ex.StackTrace;
                    error.error_inner = ex.InnerException != null ? ex.InnerException.Message : "";
                    db.tb_m_error_log.Add(error);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string RandomNumber(int length)
        {
            Random random = new Random();
            string chars = ConfigurationManager.AppSettings["mark_range"];
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public string RandomNumberPassword(int length)
        {
            Random random = new Random();
            string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public string RandomString(int length)
        {
            Random random = new Random();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public void InsertErrorLog(Exception ex, string className, string methodName)
        {
            try
            {
                System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
                string strMessage = ex.Message;
                string strDetail = ex.StackTrace.ToString();

                using (cn_systemEntities db = new cn_systemEntities())
                {
                    tb_r_error_log error = new tb_r_error_log();
                    error.error_message = strMessage;
                    error.error_class = className;
                    error.error_detail = strDetail;
                    error.error_method = methodName;
                    db.tb_r_error_log.Add(error);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
    public class JsonResultItem
    {
        public string result { get; set; }
    }
}
