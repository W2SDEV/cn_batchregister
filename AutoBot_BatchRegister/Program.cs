﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoBot_BatchRegister.Model;
using AutoBot_BatchRegister.Utility;

namespace AutoBot_BatchRegister
{
    class Program
    {
        private UtilityClass ut = new UtilityClass();
        private UFAAPI UFAAPI = new UFAAPI();
        private static Timer timer;
        private static bool threadLock;
        private static Program pg = new Program();
        public static string interval = ConfigurationManager.AppSettings["interval"];
        static void Main(string[] args)
        {
            Console.WriteLine("start register...");
            var autoEvent = new AutoResetEvent(false);
            int elapsed = !string.IsNullOrEmpty(interval) ? Convert.ToInt32(interval) : 60000;
            timer = new Timer(pg.TimerCallback, false, 0, elapsed);
            autoEvent.WaitOne();

            //pg.main();
        }
        private void TimerCallback(Object o)
        {
            if (!threadLock)
            {
                
                Console.WriteLine("-------------------------------------------------");
                Console.WriteLine("start time : " + DateTime.Now.ToString());

                //main register
                try
                {
                    using (cn_systemEntities db = new cn_systemEntities())
                    {
                        bool gameufa = db.tb_m_game.Where(x => x.game.Equals("UFA")).Select(x => x.active.Value).FirstOrDefault();

                        if (gameufa == true)
                        {
                            var usercn = db.tb_m_customer_user.Where(x => (x.user_ufabet == string.Empty || x.user_ufabet == null) && (x.credit_balance > 0)).Select(p => p.user_cn).ToList();
                            if (usercn.Count > 0)
                            {
                                threadLock = true;

                                foreach (var item in usercn)
                                {
                                    #region API UFA
                                    var user = db.tb_m_customer_user.Where(x => x.user_cn == item).FirstOrDefault();

                                    string user_ufabet = String.Empty;
                                    string new_user_ufabet = String.Empty;
                                    string queue_user_ufabet = String.Empty;
                                    string first_deposit = String.Empty;
                                    string password = String.Empty;
                                    string contact = String.Empty;

                                    new_user_ufabet = UFAAPI.NewUser();
                                    queue_user_ufabet = new_user_ufabet;
                                    first_deposit = "0";

                                    password = ut.RandomString(4) + ut.RandomNumberPassword(4);
                                    contact = user.name_th + " " + user.lastname_th + "-" + user.account_no + "-" + user.bank;

                                    bool register_ufabet = UFAAPI.Register(user.agent, new_user_ufabet, password, first_deposit, contact);
                                    Console.WriteLine("register user ufabet : " + item  + " is " + register_ufabet.ToString());
                                    if (register_ufabet == true)
                                    {
                                        new_user_ufabet = user.agent + new_user_ufabet;

                                        string new_password = ut.RandomNumberPassword(4) + ut.RandomString(4);
                                        bool reset_ufabet = UFAAPI.ResetPassword(user.agent, new_user_ufabet, password, new_password);
                                        Console.WriteLine("register reset password ufabet : " + item + " is " + reset_ufabet.ToString());
                                        if (reset_ufabet == true)
                                        {
                                            user.user_ufabet = new_user_ufabet;
                                            user.password_ufabet = new_password;    
                                        }
                                        else
                                        {
                                            user.user_ufabet = new_user_ufabet;
                                            user.password_ufabet = password;
                                        }
                                    }

                                    var deleteQueue = db.tb_r_register_queue.Where(f => f.user_ufabet.Equals(queue_user_ufabet)).FirstOrDefault();
                                    if (deleteQueue != null)
                                    {
                                        db.tb_r_register_queue.Remove(deleteQueue);
                                        db.SaveChanges();
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ut.ErrorLog(ex);
                    ut.InsertErrorLog(ex, MethodBase.GetCurrentMethod().ReflectedType.Name, MethodBase.GetCurrentMethod().Name);
                    Console.WriteLine(ex.Message);
                }

                Console.WriteLine("end time : " + DateTime.Now.ToString());     
                threadLock = false;
            }        
        }


        private void main()
        {
            try
            {
                using (cn_systemEntities db = new cn_systemEntities())
                {
                    bool gameufa = db.tb_m_game.Where(x => x.game.Equals("UFA")).Select(x => x.active.Value).FirstOrDefault();

                    if (gameufa == true)
                    {
                        var usercn = db.tb_m_customer_user.Where(x => (x.user_ufabet == string.Empty || x.user_ufabet == null) && (x.credit_balance > 0)).Select(p => p.user_cn).ToList();
                        if (usercn.Count > 0)
                        {
                            foreach (var item in usercn)
                            {
                                #region API UFA
                                var user = db.tb_m_customer_user.Where(x => x.user_cn == item).FirstOrDefault();

                                string user_ufabet = String.Empty;
                                string new_user_ufabet = String.Empty;
                                string queue_user_ufabet = String.Empty;
                                string first_deposit = String.Empty;
                                string password = String.Empty;
                                string contact = String.Empty;

                                new_user_ufabet = UFAAPI.NewUser();
                                queue_user_ufabet = new_user_ufabet;
                                first_deposit = "0";

                                password = ut.RandomString(4) + ut.RandomNumberPassword(4);
                                contact = user.name_th + " " + user.lastname_th + "-" + user.account_no + "-" + user.bank;

                                bool register_ufabet = UFAAPI.Register(user.agent, new_user_ufabet, password, first_deposit, contact);
                                Console.WriteLine("register user ufabet : " + item + " is " + register_ufabet.ToString());
                                if (register_ufabet == true)
                                {
                                    new_user_ufabet = user.agent + new_user_ufabet;

                                    string new_password = ut.RandomNumberPassword(4) + ut.RandomString(4);
                                    bool reset_ufabet = UFAAPI.ResetPassword(user.agent, new_user_ufabet, password, new_password);
                                    Console.WriteLine("register reset password ufabet : " + item + " is " + reset_ufabet.ToString());
                                    if (reset_ufabet == true)
                                    {
                                        user.user_ufabet = new_user_ufabet;
                                        user.password_ufabet = new_password;
                                    }
                                    else
                                    {
                                        user.user_ufabet = new_user_ufabet;
                                        user.password_ufabet = password;
                                    }
                                }

                                var deleteQueue = db.tb_r_register_queue.Where(f => f.user_ufabet.Equals(queue_user_ufabet)).FirstOrDefault();
                                if (deleteQueue != null)
                                {
                                    db.tb_r_register_queue.Remove(deleteQueue);
                                    db.SaveChanges();
                                }
                                #endregion
                            }
                        }
                    }
                }
               
            }
            catch (Exception ex)
            {
                ut.ErrorLog(ex);
                ut.InsertErrorLog(ex, MethodBase.GetCurrentMethod().ReflectedType.Name, MethodBase.GetCurrentMethod().Name);
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();

        }
    }
}
